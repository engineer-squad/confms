ConfMS
# ConfMS
A tool supporting organization of scientific conferences in academic environments.

![Dashboard](.images/dashboard.png)

## Features
- organizers work synchronization by providing an access to shared conference administration panel,
- defining conference metadata,
- organizers privileges management by determinig a composition of organizing committee,
- signing and management of participants,
- support for conference planning by providing intuitive interface for agenda creating, events administration etc.,
- support for communication between carious user groups, sending emails to these groups,
- exporting data from application in svg format.

## Architecture and technologies
Designed based on SPA (Single Page Application) model. Server side is written in PHP using Lumen framework. Client application in written in Javascript ES6, using React library for user interface and Mobx for managing application state. Communication bettween modules takes place via HTTP protocol using REST API. NoSQL database (MongoDB) is used to persist data.

## Build and run
Detailed instructions are provided in subfolders.

## Background
Project made as a part of Bachelor of Engineering thesis carried out by students of Computer Science at Poznań University Of Technology, Poland.

Authors:
- Marcin Piniarski
- Adam Szrama
- Krzysztof Jurkiewicz
- Jakub Łuczak
## Screenshots
![Dashboard](.images/1.png)
![Dashboard](.images/2.png)
![Dashboard](.images/3.png)
![Dashboard](.images/4.png)